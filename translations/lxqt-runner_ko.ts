<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>ConfigureDialog</name>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="14"/>
        <source>Runner Settings</source>
        <translation>실행도구 설정</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="20"/>
        <source>Appearance</source>
        <translation>모양새</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="26"/>
        <source>Positioning:</source>
        <translation>위치 지정:</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="36"/>
        <source>Show on:</source>
        <translation>표시 대상:</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="46"/>
        <source>Show history first</source>
        <translation>기록 먼저 표시</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="53"/>
        <source>Store/show history</source>
        <translation>기록 저장/표시</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="60"/>
        <source>&lt;b&gt;Note&lt;/b&gt;: The size of top-level widgets are constrained to 2/3 of the desktop&apos;s height and width.</source>
        <translation>&lt;b&gt;참고&lt;/b&gt;: 최상위 위젯의 크기는 바탕 화면 높이와 너비의 2/3로 제한됩니다.</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="63"/>
        <source>Show list with:</source>
        <translation>다음으로 목록 표시:</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="70"/>
        <source> item(s)</source>
        <translation> 개 항목</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.ui" line="90"/>
        <source>Shortcut:</source>
        <translation>단축키:</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.cpp" line="61"/>
        <source>Top edge of the screen</source>
        <translation>화면 상단 가장자리</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.cpp" line="62"/>
        <source>Center of the screen</source>
        <translation>화면 중앙</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.cpp" line="67"/>
        <source>Focused screen</source>
        <translation>포커스 된 화면</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.cpp" line="71"/>
        <source>Always on screen %1</source>
        <translation>항상 %1 화면에 표시</translation>
    </message>
    <message>
        <location filename="../configuredialog/configuredialog.cpp" line="80"/>
        <source>Reset</source>
        <translation>재설정</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="26"/>
        <source>Application launcher </source>
        <translation>응용프로그램 실행도구 </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="105"/>
        <source>Configure</source>
        <translation>구성하기</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="109"/>
        <source>Clear History</source>
        <translation>기록 지우기</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="374"/>
        <source>Show/hide runner dialog</source>
        <translation>실행도구 대화창 표시/숨김</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../providers.cpp" line="445"/>
        <source>History</source>
        <translation>기록</translation>
    </message>
    <message>
        <location filename="../providers.cpp" line="846"/>
        <source>Copy calculation result to clipboard</source>
        <translation>클립보드에 계산 결과 복사</translation>
    </message>
</context>
</TS>
